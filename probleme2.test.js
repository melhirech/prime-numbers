const maxSumOfPrimes = require('./Probleme2').maxPrime

/**
 * Test: the prime wich is the sum of most consecutive primes below LIMIT
 */
const LIMIT = 1000000

console.log(`The biggest prime number which is the sum
 of most consecutive primes below ${LIMIT} is ${maxSumOfPrimes(LIMIT)}`)