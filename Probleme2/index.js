const generatePrimes = require('../Probleme1').generatePrimes
const binarySearch = require('../helpers').binarySearch


/**
 * returns max prime made of the sum of consecutive primes smaller than 'limit'
 * @param {Number} limit 
 */
const maxPrime = (limit) => {
    const primes = generatePrimes(limit) //generate array of primes up to limit
    let sum = 0, length = 0;

    while (sum < limit) { //calculate the greatest sum of primes less than limit
        let tmpSum = sum + primes[length]
        if (tmpSum >= limit) break;
        sum = tmpSum;
        length++;
    }

    let targetPrime = 0
    for (; length > 1; length--) {
        targetPrime = findPrime(primes, limit, sum, length - 1) //check if sum is a prime in our array
        if (targetPrime > 0) break;
        sum -= primes[length - 1] //subtract one prime (greatest prime) from sum
    }

    return targetPrime;
}

const findPrime = (primes, maxSum, sum, lastIndex) => {
    let result = 0
    let index = lastIndex + 1
    for (let firstIndex = 0; lastIndex < primes.length && sum <= maxSum; firstIndex++ , lastIndex++) {
        index = binarySearch(primes,sum) //check if sum is a prime that exists in our array
        if (index > 0) {
            result = primes[index] //Prime found
            return result
        }; 
        if (index < 0) index = ~index;
        sum = sum - primes[firstIndex] + primes[lastIndex + 1];
    }
    return result;
}

module.exports = { maxPrime }