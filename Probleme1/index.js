
/**
 * Generates array of primes up to LIMIT
 * @param {*} limit 
 */
const generatePrimes = (limit) => {
    const prime = []
    const primesList = []

    //populate an array with all possible primes up to LIMIT
    for (var i = 0; i <= limit; i++) {
        prime.push(true);
    }

    let p = 2
    while (p * p <= limit) {
        if (prime[p] === true) {
            for (let i = p * p; i <= limit; i += p) {
                prime[i] = false
            }
        }
        p += 1
    }

    for (let i = 2; i <= limit; i++) {
        if (prime[i]) primesList.push(i)
    }

    return primesList
}


/**
 * Sum of primes
 * @param {Number} limit 
 */
const sumPrimes = (limit) => {
    const primes = generatePrimes(limit)
    return primes.reduce((sum, nextPrime) => sum + nextPrime, 0)
}

module.exports = { generatePrimes, sumPrimes }