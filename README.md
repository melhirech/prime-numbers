# PRIME NUMBERS

## Install it locally

* Make sure you have #Prerequisites installed
* Clone this repo:
```
git clone https://melhirech@bitbucket.org/melhirech/prime-numbers.git
```

### Prerequisites

* Node.js -- 8.x or later
* Git


## Running the tests

* Problem 1:
```
node probleme1.test.js
```
* Problem 2:
```
node probleme2.test.js
```

## Test example

* Problem 1:
```
λ node probleme1.test.js
The sum of all prime numbers below 2000000 is 142913828922
```
* Problem 2:
```
λ node probleme2.test.js
The biggest prime number which is the sum
 of most consecutive primes below 1000000 is 997651
```