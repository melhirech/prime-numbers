const sumPrimes = require('./Probleme1').sumPrimes


/**
 * Test: sum of all primes below LIMIT
 */
const LIMIT = 2000000
console.log(`The sum of all prime numbers below ${LIMIT} is ${sumPrimes(LIMIT)}`)